//
//  BonusDagger.m
//  Cocos2DSimpleGame
//
//  Created by vmware on 11/23/13.
//  Copyright (c) 2013 Razeware LLC. All rights reserved.
//

#import "BonusDagger.h"
#import "DaggerFactory.h"

@implementation BonusDagger

+(id) incomingObjectWithHost: (id<IncomingObjectHost>) host {
    return [[self alloc] initWithHost: host];
}

-(id) initWithHost: (id <IncomingObjectHost>) host {
    self = [super initWithHost: host life: 1 andSprite:@"daga.png"];
    
    [super setRandomStartupPosition];
    [super setRandomSpeedLinearWithMaxDuration:8 andMinDuration:4];
    
    [_sprite runAction:[CCRepeatForever actionWithAction:[CCRotateBy actionWithDuration:5.0 angle:360]]];
    
    return self;
}

-(void) movementFinished {
    [_host incPoints: 50000 fromObject: self];
    [_host setProjectileFactory: [[DaggerFactory alloc] init]];
    [_host incomingObjectExpired: self];
}

-(Boolean) hit:(Projectile *)projectile {
    
    if ([super hit: projectile]) {
        
        CCParticleSystem* particleSystem = [[CCParticleExplosion alloc] initWithTotalParticles:100];
        
        [particleSystem setLife: 0.1];
        [particleSystem setLifeVar: 0.025];
        [particleSystem setSpeed: 280.0];
        [particleSystem setGravity: ccp(0.0,-1000.0)];
        [particleSystem setTexture: [[CCTextureCache sharedTextureCache] addImage:@"polvo.png" ] ];
        [particleSystem setStartColor: ccc4f(0.8f, 0.8f, 0.8f, 0.8f)];
        [particleSystem setStartColorVar: ccc4f(0.0f, 0.0f, 0.0f, 0.0f)];
        [particleSystem setEndColorVar: ccc4f(0.0f, 0.0f, 0.0f, 0.0f)];
        [particleSystem setEndColor: ccc4f(0.8f, 0.8f, 0.8f, 0.0f)];
        
        particleSystem.position = ccpAdd(self.position,ccp(-_sprite.contentSize.width,_sprite.contentSize.height));
        [_host addChild:particleSystem];
        [_host resetCombo: self];
        
        return true;
    } else {
        return false;
    }
    
}

-(void) die {
    [_host incomingObjectExpired: self];
    [_host decPoints:5000 fromObject: self];
}

@end
