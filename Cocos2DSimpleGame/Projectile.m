//
//  Projectile.m
//  Cocos2DSimpleGame
//
//  Created by AdminMacLC01 on 10/10/13.
//  Copyright (c) 2013 Razeware LLC. All rights reserved.
//

#import "Projectile.h"
#import "cocos2d.h"
#import "SimpleAudioEngine.h"

@implementation Projectile

-(CGRect)getBoundingBox {
    return self.boundingBox;
}

-(int) getDamage {
    return _damage;
}


+ (Projectile*) projectileWithDirection:(CGPoint)offset position:(CGPoint)position host:(id<ProjectileHost>)host damage:(int)damage sprite:(NSString *)sprite andSound:(NSString *)sound {
    return [[self alloc] initWithDirection:offset position:position host:host damage:damage sprite:sprite andSound:sound];
}


- (void) movementWithDirection:(CGPoint)offset position:(CGPoint)position andVelocity:(float)velocity {
    
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    self.position = position;
    
    int realY = winSize.height + (self.contentSize.height/2);
    float ratio = (float) offset.x / (float) offset.y;
    int realX = (realY * ratio) + self.position.x;
    CGPoint realDest = ccp(realX, realY);
    
    // Determine the length of how far you're shooting
    int offRealX = realX - self.position.x;
    int offRealY = realY - self.position.y;
    float length = sqrtf((offRealX*offRealX)+(offRealY*offRealY));
    float realMoveDuration = length/velocity;
    
    self.rotation = atan2(offset.x, offset.y)*180.0/M_PI;
    NSLog(@"rotation:%f, %f",atan2(offset.x, offset.y),self.rotation);
    
    // Move projectile to actual endpoint
    [self runAction:
     [CCSequence actions:
      [CCMoveTo actionWithDuration:realMoveDuration position:realDest],
      [CCCallBlockN actionWithBlock:^(CCNode *node) {
         [_host projectileExpired: self];
         [node removeFromParentAndCleanup:YES];
     }],
      nil]];
    
}

- (void) playSound:(NSString *)sound {
    if (sound)
        [[SimpleAudioEngine sharedEngine] playEffect:@"pew-pew-lei.caf"];
}

- (id) initWithDirection:(CGPoint)offset position:(CGPoint)position host:(id<ProjectileHost>)host damage:(int)damage sprite:(NSString *)sprite andSound:(NSString *)sound {

    self = [super initWithFile: sprite];
    _host = host;
    _damage = damage;
    
    [self movementWithDirection:offset position:position andVelocity:480];
    [self playSound: sound];
    
    
    return self;
    
}

@end
