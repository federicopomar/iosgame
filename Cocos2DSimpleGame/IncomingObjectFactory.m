//
//  IncomingObjectFactory.m
//  Cocos2DSimpleGame
//
//  Created by vmware on 11/22/13.
//  Copyright (c) 2013 Razeware LLC. All rights reserved.
//

#import "IncomingObjectFactory.h"
#import "IncomingObject.h"

@implementation IncomingObjectFactoryEntry
{
    Class _incomingObjectClass;
    int _prob;
}

-(id) initWithIncomingObjectClass: (Class) incomingObjectClass andProbability: (int) prob {
    self = [super init];
    _incomingObjectClass = incomingObjectClass;
    _prob = prob;
    return self;
}

-(int) getProbability {
    return _prob;
}

-(Class) getIncomingObjectClass {
    return _incomingObjectClass;
}

@end

@implementation IncomingObjectFactory
{
    NSMutableArray* _incomingObjectClasses;
    float _baseProb;
}
-(id) initWithBaseProbability:(float)baseProb {
    self = [super init];
    _incomingObjectClasses = [NSMutableArray array];
    _baseProb = baseProb;
    return self;
}

-(int) getProbabilitySum {
    int total = 0;
    for (IncomingObjectFactoryEntry* entry in _incomingObjectClasses) {
        total += [entry getProbability];
    }
    return total;
}

-(void) addIncomingObjectClass:(Class)incomingObjectClass withProbability:(int)prob {
    IncomingObjectFactoryEntry* entry = [[IncomingObjectFactoryEntry alloc] initWithIncomingObjectClass:incomingObjectClass andProbability:prob];
    [_incomingObjectClasses addObject: entry];
}

-(IncomingObject*) createRandomIncomingObjectWithHost: (id<IncomingObjectHost>) host {
    if ([_incomingObjectClasses count] < 1) return nil;
    if (((float)(arc4random() % 10000))/10000.0 > _baseProb) return nil;
    int randval = arc4random()%[self getProbabilitySum];
    for (IncomingObjectFactoryEntry* entry in _incomingObjectClasses) {
        if (randval < [entry getProbability]) {
            return [[entry getIncomingObjectClass] performSelector: @selector(incomingObjectWithHost:) withObject: host];
        } else {
            randval -= [entry getProbability];
        }
    }
    return nil;
}
@end
