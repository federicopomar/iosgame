//
//  IncomingObjectFactory.h
//  Cocos2DSimpleGame
//
//  Created by vmware on 11/22/13.
//  Copyright (c) 2013 Razeware LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IncomingObject.h"

@interface IncomingObjectFactory : NSObject
-(id) initWithBaseProbability: (float) baseProb;
-(void) addIncomingObjectClass: (Class) incomingObjectClass withProbability: (int) prob;
-(IncomingObject*) createRandomIncomingObjectWithHost: (id<IncomingObjectHost>) host;
@end

@interface IncomingObjectFactoryEntry : NSObject
-(id) initWithIncomingObjectClass: (Class) incomingObjectClass andProbability: (int) prob;
-(int) getProbability;
-(Class) getIncomingObjectClass;
@end