//
//  SkeletonMonster.h
//  Cocos2DSimpleGame
//
//  Created by AdminMacLC01 on 10/31/13.
//  Copyright (c) 2013 Razeware LLC. All rights reserved.
//
#import "IncomingObject.h"

#import "CCSpriteBatchNode.h"
#import "CCSpriteFrame.h"
#import "CCSpriteFrameCache.h"
#import "CCAnimation.h"
#import "CCAction.h"
#import "CCActionTween.h"

@interface SkeletonMonster : IncomingObject

@end
