//
//  ZombieMonster.m
//  Cocos2DSimpleGame
//
//  Created by AdminMacLC01 on 10/3/13.
//  Copyright (c) 2013 Razeware LLC. All rights reserved.
//

#import "ZombieMonster.h"

#import "CCSpriteBatchNode.h"
#import "CCSpriteFrame.h"
#import "CCSpriteFrameCache.h"
#import "CCAnimation.h"
#import "CCAction.h"
#import "CCActionTween.h"

@implementation ZombieMonster

+(id) incomingObjectWithHost: (id<IncomingObjectHost>) host {
    return [[self alloc] initWithHost: host];
}

-(id) initWithHost: (id <IncomingObjectHost>) host {
    self = [super initWithHost: host andSpriteSheet:@"garrote" andSpriteCount:6 andDelay:0.05f andLife: 100];
    
    [super setRandomStartupPosition];
    [super setRandomSpeedZigZagWithMaxDuration:8 andMinDuration:4];
    
    return self;
}

-(void) movementFinished {
    [_host decLife: self];
    [_host decPoints: 1000 fromObject: self];
    [_host incomingObjectExpired: self];    
}

-(Boolean) hit:(Projectile *)projectile {
    
    if ([super hit: projectile]) {        
        CCParticleSystem* particleSystem = [[CCParticleExplosion alloc] initWithTotalParticles:100];
        
        [particleSystem setLife: 0.12];
        [particleSystem setLifeVar: 0.025];
        [particleSystem setSpeed: 250.0];
        [particleSystem setGravity: ccp(0.0,-1000.0)];
        [particleSystem setTexture: [[CCTextureCache sharedTextureCache] addImage:@"smalllife.png" ] ];
        
        particleSystem.position = self.position;
        [_host addChild:particleSystem];
        
        return true;
    } else {
        return false;
    }
    
}

-(void) die {
    [_host incomingObjectExpired: self];
    [_host incPoints:10000 fromObject: self];
    [_host incCombo:self];
}

@end