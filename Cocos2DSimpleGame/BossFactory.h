//
//  BossFactory.h
//  Cocos2DSimpleGame
//
//  Created by vmware on 11/22/13.
//  Copyright (c) 2013 Razeware LLC. All rights reserved.
//

#import "IncomingObjectFactory.h"

@interface BossFactory : IncomingObjectFactory
-(id) initWithBossClass: (Class) bossClass;
@end
