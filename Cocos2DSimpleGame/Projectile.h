//
//  Projectile.h
//  Cocos2DSimpleGame
//
//  Created by AdminMacLC01 on 10/10/13.
//  Copyright (c) 2013 Razeware LLC. All rights reserved.
//

#import "CCSprite.h"

@protocol ProjectileHost;

@interface Projectile : CCSprite
{
    id<ProjectileHost> _host;
    int _damage;
}

-(int) getDamage;

-(CGRect)getBoundingBox;

+ (Projectile*) projectileWithDirection: (CGPoint) offset position: (CGPoint) position host: (id<ProjectileHost>) host damage:(int)damage sprite: (NSString*) sprite andSound: (NSString*) sound;

- (void) movementWithDirection:(CGPoint)offset position:(CGPoint)position andVelocity:(float)velocity;

- (void) playSound: (NSString*) sound;

- (id) initWithDirection: (CGPoint) offset position: (CGPoint) position host: (id<ProjectileHost>) host damage:(int)damage sprite: (NSString*) sprite andSound: (NSString*) sound;

@end


@protocol ProjectileHost <NSObject>

- (void) projectileExpired: (Projectile *) projectile;

@end