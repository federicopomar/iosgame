//
//  BonusCow.m
//  Cocos2DSimpleGame
//
//  Created by AdminMacLC01 on 10/31/13.
//  Copyright (c) 2013 Razeware LLC. All rights reserved.
//

#import "BonusCow.h"

@implementation BonusCow

+(id) incomingObjectWithHost: (id<IncomingObjectHost>) host {
    return [[self alloc] initWithHost: host];
}

-(id) initWithHost: (id <IncomingObjectHost>) host {
    self = [super initWithHost: host andSpriteSheet:@"vaca" andSpriteCount:6 andDelay:0.05f andLife: 150];
    
    [super setRandomStartupPosition];
    [super setRandomSpeedLinearWithMaxDuration:8 andMinDuration:4];
    
    return self;
}

-(void) movementFinished {
    [_host incPoints: 50000 fromObject: self];
    [_host incomingObjectExpired: self];
}

-(Boolean) hit:(Projectile *)projectile {
    
    if ([super hit: projectile]) {
        
        CCParticleSystem* particleSystem = [[CCParticleExplosion alloc] initWithTotalParticles:100];
        
        [particleSystem setLife: 0.2];
        [particleSystem setLifeVar: 0.025];
        [particleSystem setSpeed: 280.0];
        [particleSystem setGravity: ccp(0.0,-1000.0)];
        [particleSystem setTexture: [[CCTextureCache sharedTextureCache] addImage:@"smalllife.png" ] ];
        
        particleSystem.position = self.position;
        [_host addChild:particleSystem];
        [_host resetCombo: self];
        
        return true;
    } else {
        return false;
    }
    
}

-(void) die {
    [_host incomingObjectExpired: self];
    [_host decPoints:5000 fromObject: self];
}

@end
