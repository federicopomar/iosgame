//
//  Monster.m
//  Cocos2DSimpleGame
//
//  Created by AdminMacLC01 on 10/3/13.
//  Copyright (c) 2013 Razeware LLC. All rights reserved.
//

#import "IncomingObject.h"

#import "CCSpriteBatchNode.h"
#import "CCSpriteFrame.h"
#import "CCSpriteFrameCache.h"
#import "CCAnimation.h"
#import "CCAction.h"
#import "CCActionTween.h"

@interface ZombieMonster : IncomingObject

@end
