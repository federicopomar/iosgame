//
//  HelloWorldLayer.h
//  Cocos2DSimpleGame
//
//  Created by Ray Wenderlich on 11/13/12.
//  Copyright Razeware LLC 2012. All rights reserved.
//


#import <GameKit/GameKit.h>

// When you import this file, you import all the cocos2d classes
#import "cocos2d.h"
#import "IncomingObject.h"
#import "Projectile.h"
#import "IncomingObjectFactory.h"
#import "BossFactory.h"
#import "ProjectileFactory.h"
#import "DaggerFactory.h"
#import "ArrowFactory.h"

// HelloWorldLayer
@interface HelloWorldLayer : CCLayerColor <IncomingObjectHost, ProjectileHost>
{
    NSMutableArray * _incomingObjects;
    NSMutableArray * _projectiles;
    NSMutableArray * _ownedLives;
    NSMutableArray * _lives;
    
    CCLabelTTF * _pointsLabel;
    CCLabelTTF * _comboLabel;
    CCLabelTTF * _levelLabel;
    
    ProjectileFactory* _projectileFactory;
    
    IncomingObjectFactory* _incomingObjectFactory;
    
    int _points;
    int _combo;
    int _level;
    
}

// returns a CCScene that contains the HelloWorldLayer as the only child
+(CCScene *) scene;

-(void) removeIncomingObject: incomingObject;
-(void) addIncomingObject: incomingObject;

-(Boolean) addLife;
-(Boolean) removeLife;

-(void) incPoints: (int) points;
-(void) decPoints: (int) points;

@end
