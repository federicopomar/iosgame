//
//  BossFactory.m
//  Cocos2DSimpleGame
//
//  Created by vmware on 11/22/13.
//  Copyright (c) 2013 Razeware LLC. All rights reserved.
//

#import "BossFactory.h"

@implementation BossFactory
{
    BOOL _bossCreated;
    Class _bossClass;
}

-(id) initWithBossClass: (Class) bossClass  {
    self = [super init];
    _bossCreated = NO;
    _bossClass = bossClass;
    return self;
}

-(IncomingObject*) createRandomIncomingObjectWithHost: (id<IncomingObjectHost>) host {
    if (_bossCreated == NO) {
        _bossCreated = YES;
        return [_bossClass performSelector:@selector(incomingObjectWithHost:) withObject: host];
    } else
        return nil;
}
@end
