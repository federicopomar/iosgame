//
//  IncomingObject.m
//  Cocos2DSimpleGame
//
//  Created by AdminMacLC01 on 10/3/13.
//  Copyright (c) 2013 Razeware LLC. All rights reserved.
//

#import "IncomingObject.h"
#import "cocos2d.h"

#import "ZombieMonster.h"
#import "SkeletonMonster.h"
#import "BonusCow.h"
#import "BonusLife.h"
#import "WolfMonster.h"
#import "DragonBossMonster.h"


@implementation IncomingObject

/*
 Sistema de particulas
 Animacion de sprite
 Parallax scroll (opcional)
 Tres malos distintos (Comportamiento diferente en movimiento)
 Un arma alternativa
 Puntaje con combo
 Niveles con dificultad incremental
 */

-(Boolean) hit: (Projectile*) projectile {
    
    CGRect target;
    
    target = self.boundingBox;
    target.size = _hitDimensions;
    
    if (CGRectIntersectsRect([projectile getBoundingBox], target)) {
        _life -= [projectile getDamage];
        if (_life <= 0)
            [self die];
        return true;
    } else {
        return false;
    }
}

-(void) die {
    
}

-(void) movementFinished {
    [_host incomingObjectExpired: self];
}

-(id) initWithHost: (id<IncomingObjectHost>)host life: (int)life {
    self = [super init];
    _hitDimensions = self.boundingBox.size;
    _host = host;
    _life = life;
    return self;
}

+(id) incomingOjectWithHost:(id<IncomingObjectHost>)host {
    return nil;
}

-(id) initWithHost:(id<IncomingObjectHost>)host life:(int)life andSprite:(NSString *)sprite {
    self = [super init];
    _sprite = [CCSprite spriteWithFile:sprite];
    _host = host;
    _life = life;
    [self addChildCentered:_sprite];
    [self setHitDimensions:_sprite.contentSize];
    return self;
}


-(id) initWithHost:(id<IncomingObjectHost>)host andSpriteSheet: (NSString*) name andSpriteCount: (int) count andDelay: (float) delay andLife: (int) life {
    self = [super init];
    _hitDimensions = self.boundingBox.size;
    _host = host;
    _life = life;
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:[NSString stringWithFormat:@"%@.plist", name]];
    _spriteSheet = [CCSpriteBatchNode batchNodeWithFile:[NSString stringWithFormat:@"%@.png", name]];    
    
    [self addChildCentered:_spriteSheet];
    NSMutableArray *walkAnimFrames = [NSMutableArray array];
    
    for (int i=0; i<count; i++) {
        [walkAnimFrames addObject:
         [[CCSpriteFrameCache sharedSpriteFrameCache] spriteFrameByName:
          [NSString stringWithFormat:@"%@%d.png",name,i]]];
    }
    
    CCAnimation *walkAnim = [CCAnimation
                             animationWithSpriteFrames:walkAnimFrames delay:delay];
    
    _sprite = [CCSprite spriteWithSpriteFrameName:[NSString stringWithFormat:@"%@0.png", name]];
    _animAction = [CCRepeatForever actionWithAction:
                   [CCAnimate actionWithAnimation:walkAnim]];
    [_sprite runAction: _animAction];
    [_spriteSheet addChild: _sprite];
    
    [self setHitDimensions: _sprite.contentSize];
    
    return self;
}

-(void) setHitDimensions:(CGSize)hitDimensions {
    _hitDimensions = hitDimensions;
}

-(void) addChildCentered:(CCNode *)node {
    NSLog(@"child size: %f, %f", node.contentSize.width, node.contentSize.height);
    node.position = ccp(-node.contentSize.width, node.contentSize.height);
    NSLog(@"child centered: %f, %f", node.position.x, node.position.y);
    return [self addChild: node];
}


-(void) setRandomStartupPosition {
    CGSize winSize = [CCDirector sharedDirector].winSize;
    int minX = _hitDimensions.width / 2;
    int maxX = winSize.width - _hitDimensions.width/2;
    int rangeX = maxX - minX;
    int actualX = (arc4random() % rangeX) + minX;
    
    // Create the monster slightly off-screen along the right edge,
    // and along a random position along the Y axis as calculated above
    self.position = ccp(actualX, winSize.height + _hitDimensions.height/2);
}

-(void) setCenterTopStartupPosition {
    CGSize winSize = [CCDirector sharedDirector].winSize;
    self.position = ccp(winSize.width/2, winSize.height + _hitDimensions.height/2);
}

-(void) setRandomSpeedLinearWithMaxDuration: (int) maxDuration andMinDuration: (int) minDuration {
    // 
    // Determine speed of the monster
    int rangeDuration = maxDuration - minDuration;
    int actualDuration = (arc4random() % rangeDuration) + minDuration;
    
    // Create the actions
    CCMoveTo* actionMove = [CCMoveTo actionWithDuration:actualDuration position:ccp(self.position.x, -_hitDimensions.height/2)];
    
    CCCallBlockN * actionMoveDone = [CCCallBlockN actionWithBlock:^(CCNode *node) {
        [self movementFinished];
    }];
    
    [self runAction:[CCSequence actions:actionMove, actionMoveDone, nil]];
}

-(void) setRandomWanderingMovementWithMaxDuration: (int) maxDuration andMinDuration: (int) minDuration {
    //
    // Determine speed of the monster
    int rangeDuration = maxDuration - minDuration;
    int actualDuration = (((arc4random() % rangeDuration) + minDuration)/4) + 1;
    CGSize winSize = [CCDirector sharedDirector].winSize;
    
    // Create the actions
    CCMoveTo* actionMoveOne = [CCMoveTo actionWithDuration:actualDuration position:ccp(arc4random() % ((u_int32_t)winSize.width), arc4random() % ((u_int32_t)winSize.height))];
    
    CCMoveTo* actionMoveTwo = [CCMoveTo actionWithDuration:actualDuration position:ccp(arc4random() % ((u_int32_t)winSize.width), arc4random() % ((u_int32_t)winSize.height))];
    
    CCMoveTo* actionMoveThree = [CCMoveTo actionWithDuration:actualDuration position:ccp(arc4random() % ((u_int32_t)winSize.width), arc4random() % ((u_int32_t)winSize.height))];
    
    CCMoveTo* actionMoveFour = [CCMoveTo actionWithDuration:actualDuration position:ccp(arc4random() % ((u_int32_t)winSize.width), -_hitDimensions.height/2)];
    
    CCCallBlockN * actionMoveDone = [CCCallBlockN actionWithBlock:^(CCNode *node) {
        [self movementFinished];
    }];
    
    [self runAction:[CCSequence actions:actionMoveOne,actionMoveTwo,actionMoveThree,actionMoveFour, actionMoveDone, nil]];
}

-(void) setRandomSpeedZigZagWithMaxDuration: (int) maxDuration andMinDuration: (int) minDuration {
    //
    // Determine speed of the monster
    int rangeDuration = maxDuration - minDuration;
    int actualDuration = (((arc4random() % rangeDuration) + minDuration)/4) + 1;
    CGSize winSize = [CCDirector sharedDirector].winSize;
    
    // Create the actions
    CCMoveTo* actionMoveOne = [CCMoveTo actionWithDuration:actualDuration position:ccp(arc4random() % ((u_int32_t)winSize.width), winSize.height*3/4)];
    
    CCMoveTo* actionMoveTwo = [CCMoveTo actionWithDuration:actualDuration position:ccp(arc4random() % ((u_int32_t)winSize.width), winSize.height*2/4)];
    
    CCMoveTo* actionMoveThree = [CCMoveTo actionWithDuration:actualDuration position:ccp(arc4random() % ((u_int32_t)winSize.width), winSize.height*1/4)];
    
    CCMoveTo* actionMoveFour = [CCMoveTo actionWithDuration:actualDuration position:ccp(arc4random() % ((u_int32_t)winSize.width), -_hitDimensions.height/2)];
    
    CCCallBlockN * actionMoveDone = [CCCallBlockN actionWithBlock:^(CCNode *node) {
        [self movementFinished];
    }];
    
    [self runAction:[CCSequence actions:actionMoveOne,actionMoveTwo,actionMoveThree,actionMoveFour, actionMoveDone, nil]];
}

@end
