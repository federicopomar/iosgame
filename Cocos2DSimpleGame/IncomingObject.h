//
//  IncomingObject.h
//  Cocos2DSimpleGame
//
//  Created by AdminMacLC01 on 10/3/13.
//  Copyright (c) 2013 Razeware LLC. All rights reserved.
//

#import "CCSprite.h"
#import "CCLayer.h"
#import "Projectile.h"
#import "CCParticleSystem.h"
#import "CCParticleBatchNode.h"
#import "CCParticleExamples.h"
#import "CCTexture2D.h"
#import "CCTextureCache.h"
#import "cocos2d.h"
#import "ProjectileFactory.h"


@protocol IncomingObjectHost;


@interface IncomingObject : CCLayer
{
    id <IncomingObjectHost> _host;
    CGSize _hitDimensions;
    CCSprite * _sprite;
    CCAction * _animAction;
    CCSpriteBatchNode * _spriteSheet;
    int _life;
}

- (Boolean) hit: (Projectile*) projectile;

- (void) die;
- (void) movementFinished;

-(id) initWithHost: (id<IncomingObjectHost>)host life: (int) life;
-(id) initWithHost: (id<IncomingObjectHost>)host life: (int) life andSprite: (NSString*) sprite;
-(id) initWithHost:(id<IncomingObjectHost>)host andSpriteSheet: (NSString*) name andSpriteCount: (int) count andDelay: (float) delay andLife: (int) life;

+(id) incomingObjectWithHost:(id<IncomingObjectHost>)host;

-(void) setRandomSpeedLinearWithMaxDuration: (int) maxDuration andMinDuration: (int) minDuration;

-(void) setRandomSpeedZigZagWithMaxDuration: (int) maxDuration andMinDuration: (int) minDuration;

-(void) setRandomWanderingMovementWithMaxDuration: (int) maxDuration andMinDuration: (int) minDuration;

-(void) setRandomStartupPosition;

-(void) setCenterTopStartupPosition;

-(void) setHitDimensions: (CGSize)hitDimensions;

-(void) addChildCentered: (CCNode*) node;

@end

@protocol IncomingObjectHost <NSObject>

- (void) incomingObjectExpired: (IncomingObject*) incomingObject;
- (Boolean) incLife: (IncomingObject *) incomingObject;
- (Boolean) decLife: (IncomingObject *) incomingObject;
- (void) incPoints: (int) points fromObject: (IncomingObject *) incomingObject;
- (void) decPoints: (int) points fromObject: (IncomingObject *) incomingObject;
- (void) incCombo: (IncomingObject*) incomingObject;
- (void) resetCombo: (IncomingObject*) incomingObject;
- (void) addChild: (CCNode*) node;
- (void) endGame: (BOOL) won fromObject: (IncomingObject*) incomingObject;
- (void) setProjectileFactory: (ProjectileFactory*) factory;
@end