//
//  HelloWorldLayer.m
//  Cocos2DSimpleGame
//
//  Created by Ray Wenderlich on 11/13/12.
//  Copyright Razeware LLC 2012. All rights reserved.
//


// Import the interfaces
#import "HelloWorldLayer.h"
#import "SimpleAudioEngine.h"
#import "GameOverLayer.h"
// Needed to obtain the Navigation Controller
#import "AppDelegate.h"

#import "WolfMonster.h"
#import "BonusCow.h"
#import "DragonBossMonster.h"
#import "BonusLife.h"
#import "ZombieMonster.h"
#import "SkeletonMonster.h"
#import "BonusDagger.h"

//#define CHEAT_MODE

#pragma mark - HelloWorldLayer

// HelloWorldLayer implementation
@implementation HelloWorldLayer

// Helper class method that creates a Scene with the HelloWorldLayer as the only child.
+(CCScene *) scene
{
    // 'scene' is an autorelease object.
    CCScene *scene = [CCScene node];
    
    // 'layer' is an autorelease object.
    HelloWorldLayer *layer = [HelloWorldLayer node];
    
    // add layer as a child to scene
    [scene addChild: layer];
    
    // return the scene
    return scene;
}

- (void) setStartingLives {
    while ([_ownedLives count] > 5) [self removeLife];
    while ([_ownedLives count] < 5) [self addLife];
}

- (Boolean) addLife {
    if ([_ownedLives count] < 10) {
        // Add life
        CCSprite * life = [CCSprite spriteWithFile:@"head.png"];
        life.position = ccp(life.contentSize.width/2 + [_ownedLives count]*life.contentSize.width, life.contentSize.height);
        [self addChild:life];
        [_ownedLives addObject:life];
        if ([_ownedLives count] == 10)
            [self setLevel: _level+1];
        return true;
    } else {
        return false;
    }
}


- (Boolean) removeLife {
    if ([_ownedLives count] > 0) {
        [self removeChild:_ownedLives[[_ownedLives count] - 1]];
        [_ownedLives removeLastObject];
        if ([_ownedLives count] == 0)
            [self endGame:NO fromObject:nil];
        return true;
    } else {
        return false;
    }
}

-(void) incomingObjectExpired: (IncomingObject*) incomingObject {
    [self removeIncomingObject: incomingObject];
}

-(void) projectileExpired:(Projectile *)projectile {
    [self removeProjectile: projectile];
}

- (void) removeProjectile: (Projectile*) projectile {
    [_projectiles removeObject:projectile];
    [self removeChild:projectile cleanup:YES];
}

- (void) removeIncomingObject: (IncomingObject*) incomingObject {
    [self removeChild:incomingObject cleanup:YES];
    [_incomingObjects removeObject:incomingObject];
}

- (void) addIncomingObject: (IncomingObject*) incomingObject {
    [_incomingObjects addObject:incomingObject];
    [self addChild: incomingObject];
}

- (void) addRandomIncomingObjects {    
    IncomingObject* incomingObject = [_incomingObjectFactory createRandomIncomingObjectWithHost: self];
    if (incomingObject)
        [self addIncomingObject: incomingObject];
}

- (void) addIncomingBonusDagger {
    IncomingObject* incomingObject = [BonusDagger incomingObjectWithHost: self];
    if (incomingObject)
        [self addIncomingObject: incomingObject];
}

- (void) decPoints:(int)points fromObject:(IncomingObject *)incomingObject {
    [self decPoints:points];
    [self resetCombo:incomingObject];
}

- (void) incPoints:(int)points fromObject:(IncomingObject *)incomingObject {
    [self incPoints:points];
}

- (void) decPoints:(int)points {
    _points -= points;
    [self updatePointsLabel];
}

- (void) incPoints:(int)points {
    _points += _combo*points;
    [self updatePointsLabel];
    
}

- (void) endGame:(BOOL) won fromObject:(IncomingObject *)incomingObject {
    CCScene* gameOverScene = [GameOverLayer sceneWithWon:won];
    [[CCDirector sharedDirector] replaceScene: gameOverScene];
}

- (void) updatePointsLabel {
    CGSize winSize = [CCDirector sharedDirector].winSize;
    _pointsLabel.string = [NSString stringWithFormat:@"%d", _points];
    _pointsLabel.position = ccp(winSize.width-_pointsLabel.contentSize.width/2, _pointsLabel.contentSize.height/2);
}

- (void) updateComboLabel {
    CGSize winSize = [CCDirector sharedDirector].winSize;
    _comboLabel.position = ccp(winSize.width-_comboLabel.contentSize.width/2, 2*(_comboLabel.contentSize.height/2));
    
    if (_combo < 2) {
        _comboLabel.visible = NO;
    } else {
        _comboLabel.string = [NSString stringWithFormat:@"(x%d)",_combo];
        _comboLabel.fontSize = (16+_combo*2);
        _comboLabel.position = ccp(winSize.width-_comboLabel.contentSize.width/2, _pointsLabel.contentSize.height+(_comboLabel.contentSize.height/2));
        _comboLabel.visible = YES;
    }
}

- (void) setLevel: (int) level {
    _level = level;
    switch (level) {
        case 0:
        case 1:
            _incomingObjectFactory = [[IncomingObjectFactory alloc] initWithBaseProbability: 0.4];
            [_incomingObjectFactory addIncomingObjectClass: [WolfMonster class] withProbability: 2];
            [_incomingObjectFactory addIncomingObjectClass: [BonusLife class] withProbability: 1];
            break;
        case 2:
            _incomingObjectFactory = [[IncomingObjectFactory alloc] initWithBaseProbability: 0.6];
            [_incomingObjectFactory addIncomingObjectClass: [WolfMonster class] withProbability: 1];
            [_incomingObjectFactory addIncomingObjectClass: [SkeletonMonster class] withProbability: 4];
            [_incomingObjectFactory addIncomingObjectClass: [BonusCow class] withProbability: 1];
            [_incomingObjectFactory addIncomingObjectClass: [BonusLife class] withProbability: 2];
            break;
        case 3:
            _incomingObjectFactory = [[IncomingObjectFactory alloc] initWithBaseProbability: 0.6];
            [_incomingObjectFactory addIncomingObjectClass: [SkeletonMonster class] withProbability: 5];
            [_incomingObjectFactory addIncomingObjectClass: [BonusCow class] withProbability: 1];
            [_incomingObjectFactory addIncomingObjectClass: [BonusLife class] withProbability: 2];
            [self addIncomingBonusDagger];
            break;
        case 4:
            _incomingObjectFactory = [[IncomingObjectFactory alloc] initWithBaseProbability: 0.7];
            [_incomingObjectFactory addIncomingObjectClass: [ZombieMonster class] withProbability: 5];
            [_incomingObjectFactory addIncomingObjectClass: [BonusCow class] withProbability: 1];
            [_incomingObjectFactory addIncomingObjectClass: [BonusLife class] withProbability: 2];
            break;
        case 5:
            _incomingObjectFactory = [[IncomingObjectFactory alloc] initWithBaseProbability: 0.8];
            [_incomingObjectFactory addIncomingObjectClass: [ZombieMonster class] withProbability: 5];
            [_incomingObjectFactory addIncomingObjectClass: [SkeletonMonster class] withProbability: 5];
            [_incomingObjectFactory addIncomingObjectClass: [BonusCow class] withProbability: 1];
            [_incomingObjectFactory addIncomingObjectClass: [BonusLife class] withProbability: 2];
            break;
        default:
            _incomingObjectFactory = [[BossFactory alloc] initWithBossClass: [DragonBossMonster class]];
            break;
    }
    [self setStartingLives];
    [self showLevelLabel];
    
}

- (void) setProjectileFactory:(ProjectileFactory *)factory {
    _projectileFactory = factory;
}

- (Boolean) incLife:(IncomingObject *)incomingObject {
    return [self addLife];
}

- (Boolean) decLife:(IncomingObject *)incomingObject {
#ifndef CHEAT_MODE
    return [self removeLife];
#else
    return true;
#endif
}

- (void) incCombo:(IncomingObject *)incomingObject {
    if (_combo < 16)
        _combo *= 2;
    [self updateComboLabel];
}

- (void) resetCombo:(IncomingObject *)incomingObject {
    _combo = 1;
    [self updateComboLabel];
}

-(void)gameLogic:(ccTime)dt {
    [self addRandomIncomingObjects];
}

-(void)showLevelLabel {
    CGSize winSize = [CCDirector sharedDirector].winSize;
    id actionFadeIn = [CCFadeIn actionWithDuration:0.5];
    id actionFadeOut = [CCFadeOut actionWithDuration:1.5];
    if (_level <= 5)
        _levelLabel.string = [NSString stringWithFormat:@"Level %d",_level];
    else
        _levelLabel.string = @"BOSS";
    _levelLabel.position = ccp(winSize.width/2,winSize.height/2);
    [_levelLabel runAction:[CCSequence actions: actionFadeIn, actionFadeOut, nil]];
    
}
 
- (id) init
{
    if ((self = [super initWithColor:ccc4(0,0,0,255)])) {

        CGSize winSize = [CCDirector sharedDirector].winSize;
        CCSprite *player = [CCSprite spriteWithFile:@"player.png"];
        CCSprite *background = [CCSprite spriteWithFile:@"ulla-game.png" ];
        background.position = ccp(background.contentSize.width/2, background.contentSize.height/2);
        player.position = ccp(winSize.width/2, player.contentSize.height/2);
        [self addChild:background];
        [self addChild:player];
        
        [self schedule:@selector(gameLogic:) interval:1.0];
        
        [self setTouchEnabled:YES];
        
        _incomingObjects = [[NSMutableArray alloc] init];
        _projectiles = [[NSMutableArray alloc] init];
        _ownedLives = [[NSMutableArray alloc] init];
        
        [self schedule:@selector(update:)];
        
        [[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"background-music-aac.caf"];
        
        
        _points = 0;
        _pointsLabel = [CCLabelTTF labelWithString:@"0" fontName:@"Arial" fontSize:18];
        _pointsLabel.color = ccc3(255,255,255);
        _pointsLabel.position = ccp(winSize.width-_pointsLabel.contentSize.width/2, _pointsLabel.contentSize.height/2);
        
        
        [self addChild:_pointsLabel];
        
        _combo = 1;
        _comboLabel = [CCLabelTTF labelWithString:@"(x2)" fontName:@"Arial" fontSize:18];
        _comboLabel.color = ccc3(255,255,255);
        [self addChild:_comboLabel];
        [self updateComboLabel];
        
        _levelLabel = [CCLabelTTF labelWithString:@"Level 1" fontName:@"Arial" fontSize:48];
        _levelLabel.color = ccc3(255,255,255);
        [self addChild:_levelLabel];
        
        _incomingObjectFactory = [[IncomingObjectFactory alloc] init];
        [self setLevel: 1];
        
        _projectileFactory = [[ArrowFactory alloc] init];
        
    }
    return self;
}

- (void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
    // Choose one of the touches to work with
    UITouch *touch = [touches anyObject];
    CGPoint location = [self convertTouchToNodeSpace:touch];
    
    
    
    // Set up initial location of projectile
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    CGPoint position = ccp(winSize.width/2, 20);
    
    // Determine offset of location to projectile
    CGPoint offset = ccpSub(location, position);
    
    // Bail out if you are shooting down or backwards
    if (offset.y <= 0) return;
    
    Projectile *projectile = [_projectileFactory projectileWithDirection:offset position:position andHost:self];
    
    // Ok to add now - we've double checked position
    [self addChild:projectile];

    [_projectiles addObject:projectile];
    
}

- (void)update:(ccTime)dt {
    
    NSMutableArray *projectilesToDelete = [[NSMutableArray alloc] init];
    for (Projectile *projectile in _projectiles) {
        
        NSMutableArray* tempIncomingObjects = [NSMutableArray arrayWithArray: _incomingObjects];
        
        Boolean objectHit = false;
        for (IncomingObject *incomingObject in tempIncomingObjects) {
            if ([incomingObject hit: projectile])
                objectHit = true;
        }
        
        if (objectHit)
            [projectilesToDelete addObject: projectile];
    }
    
    for (Projectile *projectile in projectilesToDelete) {
        [self removeProjectile: projectile];
    }
}


// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
    _incomingObjects = nil;
    _projectiles = nil;
}

#pragma mark GameKit delegate

-(void) achievementViewControllerDidFinish:(GKAchievementViewController *)viewController
{
    AppController *app = (AppController*) [[UIApplication sharedApplication] delegate];
    [[app navController] dismissModalViewControllerAnimated:YES];
}

-(void) leaderboardViewControllerDidFinish:(GKLeaderboardViewController *)viewController
{
    AppController *app = (AppController*) [[UIApplication sharedApplication] delegate];
    [[app navController] dismissModalViewControllerAnimated:YES];
}
@end
