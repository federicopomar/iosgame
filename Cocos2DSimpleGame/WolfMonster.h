//
//  WolfMonster.h
//  Cocos2DSimpleGame
//
//  Created by vmware on 11/21/13.
//  Copyright (c) 2013 Razeware LLC. All rights reserved.
//

#import "IncomingObject.h"

#import "CCSpriteBatchNode.h"
#import "CCSpriteFrame.h"
#import "CCSpriteFrameCache.h"
#import "CCAnimation.h"
#import "CCAction.h"
#import "CCActionTween.h"

@interface WolfMonster : IncomingObject

@end