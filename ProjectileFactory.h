//
//  ProjectileFactory.h
//  Cocos2DSimpleGame
//
//  Created by vmware on 11/23/13.
//  Copyright (c) 2013 Razeware LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Projectile.h"

@interface ProjectileFactory : NSObject

- (Projectile*) projectileWithDirection: (CGPoint) offset position: (CGPoint) position andHost: (id<ProjectileHost>) host;

@end
