//
//  DaggerFactory.m
//  Cocos2DSimpleGame
//
//  Created by vmware on 11/23/13.
//  Copyright (c) 2013 Razeware LLC. All rights reserved.
//

#import "DaggerFactory.h"

@implementation DaggerFactory

-(Projectile*) projectileWithDirection:(CGPoint)offset position:(CGPoint)position andHost:(id<ProjectileHost>)host {
    return [Projectile projectileWithDirection:offset position:position host:host damage:100 sprite:@"daga.png" andSound:@"pew-pew-lei.cag"];
}

@end
