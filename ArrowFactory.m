//
//  ArrowFactory.m
//  Cocos2DSimpleGame
//
//  Created by vmware on 11/23/13.
//  Copyright (c) 2013 Razeware LLC. All rights reserved.
//

#import "ArrowFactory.h"

@implementation ArrowFactory

-(Projectile*) projectileWithDirection:(CGPoint)offset position:(CGPoint)position andHost:(id<ProjectileHost>)host {
    return [Projectile projectileWithDirection:offset position:position host:host damage:50 sprite:@"arrow.png" andSound:@"pew-pew-lei.cag"];
}

@end
